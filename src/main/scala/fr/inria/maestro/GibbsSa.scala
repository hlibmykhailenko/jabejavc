package fr.inria.maestro

import org.apache.spark.graphx.Graph
import org.apache.spark.graphx.Edge
import org.apache.spark.SparkContext._
import org.apache.spark.graphx._
import java.util.Random
import fr.inria.maestro.JABEJAVCutil._
import org.apache.spark.SparkContext
import org.apache.spark.storage.StorageLevel
import org.apache.spark.SparkConf
import org.apache.log4j.Logger
import java.io.PrintWriter
import java.io.FileWriter
import java.io.File
import java.util.Date
import java.text.DateFormat
import java.text.SimpleDateFormat

object GibbsSa {
  def main(args: Array[String]): Unit = {
    val pathToGraph = args(0)
    val N = args(1).toInt
    val T0 = args(2).toDouble
    val delta = args(3).toDouble
    val initialPartitioning = args(4)
    val outFolder = args(5)
    val energy = args(6) 
    require(energy == "squared" || energy == "cc")
    val statMode = args(7) // none | step | component
    val L = args(8).toInt

    val sc = new SparkContext(new SparkConf()
      .setSparkHome(System.getenv("SPARK_HOME"))
      .setJars(SparkContext.jarOfClass(this.getClass).toList))

    var graph: Graph[Int, Int] = GraphLoader.edgeListFile(sc, pathToGraph, false,
      edgeStorageLevel = StorageLevel.MEMORY_ONLY,
      vertexStorageLevel = StorageLevel.MEMORY_ONLY,
      minEdgePartitions = N).partitionBy(PartitionStrategy.fromString(initialPartitioning))

    val loops = Graph.fromEdges(graph.edges.filter(edge => edge.srcId == edge.dstId), 0).numEdges
    require(loops == 0, "there are some loops")

    saveToFileEdgesAsTheyPartitioned(graph, outFolder + "before")
    val gibbser = new GibbsSa(sc, graph, T0, delta, initialPartitioning, N, energy, statMode, L)

    val energyInitial = gibbser.energy(graph)

    val start = System.currentTimeMillis()
    val repartitionedGraph = gibbser.repartition()
    
    val energyFinal = gibbser.energy(repartitionedGraph)
    val timeTaken = System.currentTimeMillis() - start
    
    saveToFileEdgesAsTheyPartitioned(repartitionedGraph, outFolder + "after")

    val out = new PrintWriter(new FileWriter(new File(outFolder + "stat")));

    out.println(new SimpleDateFormat("hh:mm:ss dd MM yyyy").format(new Date()))
    out.println(s"N: $N, T0: $T0, delta: $delta, initial: $initialPartitioning L: $L")
    out.println(s"finisshed for $timeTaken ms")
    out.println(s"path: $pathToGraph")
    out.println(s"Energy went from $energyInitial to $energyFinal")
    

    out.close
  }

}

case class ED(var owner: Int, var opinions: Array[Int]) extends Serializable {
  def ownOpinion = opinions(owner)
  def updateOwnOpinion(color: Int) {
    opinions(owner) = color
  }
}

case class LS(var owner: Int, var values: Array[Double]) extends Serializable

case class BS(var owner: Int, var values: Array[Boolean]) extends Serializable

case class STAT(var swappedNeg: Int = 0, var swappedPos: Int = 0, var changedNeg: Int = 0, var changedPos: Int = 0, var accepted: Int = 0, var swapAttempt: Int = 0, var changeAttempt: Int = 0, var round: Int = 0, var T: Double = 0.0, var energy: Double = 0.0, var N: Int = 1) extends Serializable {
  def acceptanceRate = accepted.toDouble / ((N - 1) * N)
  def changed = changedNeg + changedPos
  def swapped = swappedNeg + swappedPos
  def changeRate = changedPos.toDouble / changeAttempt
  def swapRate = swappedPos.toDouble / swapAttempt

  override def toString = {
    "round: " + round + "\n" +
      "T: " + T + "\n" +
      "swapped: " + swapped + " (negative: " + swappedNeg + ", positive: " + swappedPos + ") " + "\n" +
      "swapAttempt: " + swapAttempt + "\n" +
      "swapRate: " + swapRate + "\n" +
      "changed: " + changed + " (negative: " + changedNeg + ", positive: " + changedPos + ") " + "\n" +
      "changeAttempt: " + changeAttempt + "\n" +
      "changeRate: " + changeRate + "\n" +
      "accepted: " + accepted + "\n" +
      "acceptanceRate: " + acceptanceRate + "\n" +
      "N: " + N + "\n"

  }

}

class GibbsSa(sc: SparkContext, val graph: Graph[Int, Int], T0: Double, delta: Double, initialPartitioning: String, N: Int, energy: String, statMode: String, L : Int) extends Serializable {

  @transient implicit val logger = Logger.getLogger("gibbs");
  def energy(graph: Graph[Int, Int]): Double = {
    val edges = graph.edges.mapPartitionsWithIndex((index, iterator) => {
      iterator.map(e => new Edge(e.srcId, e.dstId, index))
    }, preservesPartitioning = true)
    val g = Graph.fromEdges[Int, Int](edges, 0)
    var actualVertices = g.mapReduceTriplets(triplet => {
      val msg = Map(triplet.attr -> 1)
      Iterator((triplet.srcId, msg), (triplet.dstId, msg))
    }, (a: Map[Int, Int], b: Map[Int, Int]) => {
      zipMaps(a, b)
    })
    val g1 = g.outerJoinVertices(actualVertices)((id: VertexId, oldvalue: Int, newvalue: Option[Map[Int, Int]]) => newvalue.get)
    val sum = g1.vertices.map(vertex => energyOfVertex(vertex._2)).reduce(_ + _)
    val res = normalize(sum)
    logger.debug("ENERGY " + res)
    res
  }

  def energyOwn(graph: Graph[VD, ED]): Double = {
    val vertices = graph.mapReduceTriplets(triplet => {
      val msg = Map(triplet.attr.ownOpinion -> 1)
      Iterator((triplet.srcId, msg), (triplet.dstId, msg))
    }, (a: Map[Int, Int], b: Map[Int, Int]) => {
      zipMaps(a, b)
    })
    normalize(vertices.map(v => energyOfVertex(v._2)).reduce(_ + _))
  }

  def energyAccording(graph: Graph[VD, ED], component: Int): Double = {
    normalize(graph.vertices.map(x => energyOfVertex(x._2(component))).reduce(_ + _)) 
  }

  def energyOfVertex: (Map[Int, Int] => Double) = energy match {
    case "squared" => energyOfVertexSquared _
    case "cc"      => energyOfVertexCC _
  }

  def deltaChange = energy match {
    case "squared" => deltaEnergyChangeSquared _
    case "cc"      => deltaEnergyChangeCC _
  }

  def deltaSwap = energy match {
    case "squared" => deltaEnergySwapSquared _
    case "cc"      => deltaEnergySwapCC _
  }

  def normalize = energy match {
    case "squared" => normalizeSquared _
    case "cc"      => normalizeCC _
  }

  def normalizeSquared(value: Double): Double = {
    val C1 = 1.0 / (2 * numEdges * (1 - 1 / N))
    require((2 * numEdges - value) > 0, "but it is " + (2 * numEdges - value)) 
    val T1 = C1 * (2 * numEdges - value)
    T1
  }

  def normalizeCC(value: Double): Double = {
    value / (numVertices * N)
  }

  def energyOfVertexSquared(data: Map[Int, Int]) = {
    data.values.map(value => value * value).sum.toDouble / data.values.sum
  }

  def energyOfVertexCC(data: Map[Int, Int]) = {
    val d = data.values.sum
    val vs = for (c <- data.keys) yield indicative(0 < data.get(c).get && data.get(c).get < d)
    vs.sum
  }

  def indicative(value: Boolean) = if (value) 1.0 else 0.0

  type Triplet = EdgeTriplet[VD, ED]
  type VD = Array[Map[Int, Int]]

  val numEdges = graph.numEdges
  val numVertices = graph.numVertices
  
  val probToSwap = 0.5

  def computeT(round: Int): Double = T0 - delta * round

  def repartition(): Graph[Int, Int] = {
    val rand = new Random(System.nanoTime())
    
    var G: Graph[VD, ED] = initiateED(graph)

    var round = 0
    var T = computeT(round)

    G = repartitionBasedOnOwnership(G) //.cache

    var zeroNotPerformed = true
    while (T > 0 || zeroNotPerformed) {

      if (T <= 0) {
        zeroNotPerformed = false
        T = 0.0
      }

      G = updateVD(G) //.cache

      logStat(G, round)

      G.cache()

      G.triplets.cache()

      val iterated = G.triplets.mapPartitionsWithIndex((index, iterator) => {
        val triplets = iterator.toArray
        val pair = sa(T, triplets, index, round)
        val updatedTriplets = pair._1
        val stat = pair._2
        val l2 = computeL2(updatedTriplets)
        Iterator((updatedTriplets, l2, stat))
      }, preservesPartitioning = true).cache()
      iterated.count()

      val triplets = iterated.flatMap(_._1).cache()
      val L2s = iterated.map(_._2).collect.sortBy(x => x.owner)
      val stat = iterated.map(_._3).reduce((s1, s2) => {
        s1.swappedNeg += s2.swappedNeg
        s1.swappedPos += s2.swappedPos
        s1.swapAttempt += s2.swapAttempt
        s1.changedNeg += s2.changedNeg
        s1.changedPos += s2.changedPos
        s1.changeAttempt += s2.changeAttempt
        s1
      })
      stat.N = N
      stat.round = round
      stat.T = T

      val edges = triplets.map(x => Edge(x.srcId, x.dstId, x.attr)).cache()
      G = Graph.fromEdges(edges, null)
      G.numEdges

      G = updateVD(G) //.cache()  

      val L1s = G.triplets.mapPartitionsWithIndex((index, iterator) => {
        val l1 = computeL1(iterator.toArray)
        Iterator(l1)
      }, preservesPartitioning = true).collect.sortBy(x => x.owner)

      L1s.length

      val alphas = (L1s zip L2s).map(x => {
        computeAlphas(T, x._1, x._2)
      })

      val bs = tossAlphas(alphas, rand)

      
      logger.debug("L1 + L2: " + L1s(0).values.zip(L2s(0).values).map(x => x._1 + x._2).mkString(","))
      logger.debug("(L1 + L2)/T: " + L1s(0).values.zip(L2s(0).values).map(x => (x._1 + x._2)/T).mkString(","))
      logger.debug("ALPHAS: " + alphas(0).values.mkString(","))
      
      val trus = bs.map(x => x.values.count(p => p)).sum
      stat.accepted = trus

      logger.debug(stat)
      G = G.mapEdges((index, iterator) => {
        iterator.map(edge => {
          for (p <- bs(edge.attr.owner).values.zipWithIndex) {
            val decision = p._1
            val index = p._2
            if (decision) {
              require(edge.attr.owner != index)
              edge.attr.opinions(index) = edge.attr.opinions(edge.attr.owner)
            }
          }
          edge.attr
        })
      })
      G.numEdges

//      logger.debug("should be diff " + G.vertices.first._2.mkString(", "))
      
      round += 1
      T = computeT(round)
      G.unpersistVertices(true)
      G.triplets.unpersist(true)
      iterated.unpersist(true)
      edges.unpersist(true)
      triplets.unpersist(true)
    }

    logStat(G, round)

    G = repartitionBasedOnOwnOpinion(G)

    Graph.fromEdges(G.edges.map(edge => new Edge(edge.srcId, edge.dstId, 1)), 0)
  }

  def logStat(G: Graph[VD, ED], round: Int) {
    if (statMode == "step") {
      val e = energyOwn(G)
      logger.debug(s"ENERGY round $round:" + e)
    } else if (statMode == "component") {
      val e = energyOwn(G)
      val es = for (c <- 0 until N) yield {
        energyAccording(G, c)
      }
      logger.debug(s"ENERGY round $round:" + e + ", " + es.mkString(", "))
    }

  }

  def repartitionBasedOnOwnership(G: Graph[VD, ED]) = {
    val edges = G.edges.map(x => {
      val newPartitionID = x.attr.owner
      (newPartitionID, x)
    }).partitionBy(new IdentityPartitioner(N)).map(x => x._2)
    Graph.fromEdges[VD, ED](edges, null)
  }

  def repartitionBasedOnOwnOpinion(G: Graph[VD, ED]) = {
    val edges = G.edges.map(x => {
      val newPartitionID = x.attr.ownOpinion
      (newPartitionID, x)
    }).partitionBy(new IdentityPartitioner(N)).map(x => x._2)
    Graph.fromEdges[VD, ED](edges, null)
  }

  def tossAlphas(alphas: Array[LS], rand : Random): Array[BS] = {
    val r = for (i <- 0 until N) yield {
      val newBooleans = alphas(i).values.map(prob => {
        rand.nextDouble() <= prob
      })
      BS(alphas(i).owner, newBooleans)
    }

    r.toArray
  }

  def computeAlphas(T: Double, l1s: LS, l2s: LS): LS = {
    require(l1s.owner == l2s.owner)
    val values = for (i <- 0 until N) yield if (i == l1s.owner) {
      0
    } else {
      val l1 = l1s.values(i)
      val l2 = l2s.values(i)
      val alpha = computeAlpha(T, l1, l2)
      alpha
    }

    LS(l1s.owner, values.toArray)
  }

  def computeAlpha(T: Double, l1: Double, l2: Double): Double = {
    math.min(1, math.pow(math.E, (-1 / T) * (l1 + l2)))
  }

  def initiateED(graph: Graph[Int, Int]) = {
    val temp = Graph.fromEdges(graph.partitionBy(PartitionStrategy.fromString(initialPartitioning))
      .edges.mapPartitionsWithIndex((index, iterator) => {
        val rand = new Random(System.nanoTime())
        iterator.map(edge => {
          val owner = math.abs(rand.nextInt() % N)
          val opinions = List.fill(N)(index).toArray
          Edge(edge.srcId,
            edge.dstId,
            ED(owner, opinions))
        })
      }, preservesPartitioning = true), Array[Map[Int, Int]]())
    temp
  }

  def updateVD(G: Graph[VD, ED]): Graph[VD, ED] = {
    var actualVertices = G.mapReduceTriplets(triplet => {
      val msg1 = triplet.attr.opinions.map(op => Map(op -> 1))
      //      val msg2 = triplet.attr.opinions.map(op => Map(op -> 1))
      Iterator((triplet.dstId, msg1), (triplet.srcId, msg1))
    }, (a: Array[Map[Int, Int]], b: Array[Map[Int, Int]]) => {
      require(a.length == b.length)
      val newAr = new Array[Map[Int, Int]](N)
      for (i <- 0 until N) {
        newAr(i) = zipMaps(a(i), b(i))
      }
      newAr
    })
    require(actualVertices.count() == numVertices, actualVertices.count() + " " + numVertices)
    var G1 = Graph.fromEdges(G.edges, null).outerJoinVertices(actualVertices)((id: VertexId, oldvalue: VD, newvalue: Option[VD]) => {
      require(newvalue.isDefined, id)
      newvalue.get
    })
    G1
  }

  def computeL2(lst: Array[EdgeTriplet[VD, ED]]): LS = {
    val i = lst(0).attr.owner
    val energyForth = computeEnergy(lst, i, i)
    val values = for (j <- 0 until N) yield {
      if (i == j) 0.0 else {
        val energyThird = computeEnergy(lst, j, i)
        (energyThird - energyForth)
      }
    }
    LS(i, values.toArray)
  }

  def computeL1(lst: Array[EdgeTriplet[VD, ED]]): LS = {
    val i = lst(0).attr.owner
    val values = for (j <- 0 until N) yield {
      if (i == j) 0.0 else {
        val energyFirst = computeEnergy(lst, i, j)
        val energySecond = computeEnergy(lst, j, j)
        (energyFirst - energySecond)
      }
    }
    LS(i, values.toArray)
  }

  def computeEnergy(lst: Array[EdgeTriplet[VD, ED]], pointOfViewToI: Int, pointOfViewToMinusI: Int): Double = {
    val vertices = if (pointOfViewToI != pointOfViewToMinusI) {
      val vertcs = lst.flatMap(triplet => Iterator((triplet.srcId, triplet.srcAttr(pointOfViewToMinusI)),
        (triplet.dstId, triplet.dstAttr(pointOfViewToMinusI)))).toSet.toMap.map(x => (x._1, collection.mutable.Map(x._2.toSeq: _*)))

      lst.map(triplet => {
        if (triplet.attr.opinions(pointOfViewToI) != triplet.attr.opinions(pointOfViewToMinusI)) {
          changeColorInfo(vertcs.get(triplet.srcId).get, triplet.attr.opinions(pointOfViewToMinusI), triplet.attr.opinions(pointOfViewToI))
          changeColorInfo(vertcs.get(triplet.dstId).get, triplet.attr.opinions(pointOfViewToMinusI), triplet.attr.opinions(pointOfViewToI))
        }
        triplet
      }).length

      vertcs

    } else {
      lst.flatMap(triplet => Iterator((triplet.srcId, triplet.srcAttr(pointOfViewToI)), (triplet.dstId, triplet.dstAttr(pointOfViewToI)))).toSet
    }
    val res = vertices.map(pair => energyOfVertex(pair._2.toMap)).reduce(_ + _)
    normalize(res)
  }

  def changeColorInfo(map: collection.mutable.Map[Int, Int], from: Int, to: Int) {
    require(0 <= from && from < N, "not a color, saad")
    require(0 <= to && to < N, "not a color, saad")
    var op = map.get(from)
    require(op.isDefined, "not defined " + map + " " + from + " " + to + " where N " + N)
    require(op.get != 0, "wrong info " + op.get)
    if (op.get == 1) {
      map.remove(from)
    } else { // op.get > 1
      map.put(from, op.get - 1)
    }
    map.put(to, map.getOrElse(to, 0) + 1)
  }

  def edgeTripletToStr(triplet: EdgeTriplet[VD, ED]): String = {
    "\n" +
      "srcAttr: " + triplet.srcAttr(triplet.attr.owner) + "\n" +
      "dstAttr: " + triplet.dstAttr(triplet.attr.owner) + "\n" +
      "attr owner: " + triplet.attr.owner + "\n" +
      "own opinion: " + triplet.attr.ownOpinion + "\n" +
      "opinions: " + triplet.attr.opinions.mkString(", ")
  }

  def copyTriplet(triplet: EdgeTriplet[VD, ED], round: Int): EdgeTriplet[VD, ED] = {

    val newTriplet = new EdgeTriplet[VD, ED]

    newTriplet.srcId = triplet.srcId
    newTriplet.dstId = triplet.dstId

    val opinions = new Array[Int](N)
    for (i <- 0 until N) {
      opinions(i) = triplet.attr.opinions(i)
    }

    newTriplet.attr = ED(triplet.attr.owner, opinions)

    newTriplet
  }

  def sa(T: Double, lst: Array[EdgeTriplet[Array[Map[Int, Int]], ED]], index: Int, round: Int): (Array[EdgeTriplet[Array[Map[Int, Int]], ED]], STAT) = {
    implicit val rand = new Random(System.nanoTime())

    var stat = STAT()
    val opinions = lst
      .flatMap(triplet => Iterator((triplet.srcId, triplet.srcAttr), (triplet.dstId, triplet.dstAttr)))
      .groupBy(pair => pair._1)
      .map(group => {
        val r = group._2.head
        val ops = r._2.map(imap => collection.mutable.Map(imap.toSeq: _*))
        (r._1, ops)
      })

    opinions.filter(op => true)

    opinions.size

    val newLst = lst.map(triplet => copyTriplet(triplet, round)) //.map(triplet => { triplet.attr.lastRound = round; triplet })

    val swapEnergy = deltaSwap(T, newLst, opinions)
    val changeEnergy = deltaChange(T, newLst, opinions)
    val swapColors = swapColorsPure(index, opinions)_
    val changeColor = changePure(index, opinions)_
    for (i <- 0 until L) {
      if (rand.nextDouble() < probToSwap) {
        stat.swapAttempt += 1
        val tt = randomNotConnectedTriplets(newLst, rand)
        if (tt.isDefined) {
          val (a, b) = tt.get
          if (a.attr.ownOpinion != b.attr.ownOpinion) {
            val delta = swapEnergy(a, b)
            if (delta < 0) {
              swapColors(a, b)
              stat.swappedNeg += 1
            } else {
              val P = probability(delta, T)
              applyWithProbability(P, {
                swapColors(a, b)
                stat.swappedPos += 1
              })
            }
          }
        }
      } else { // tries to change 
        stat.changeAttempt += 1
        val triplet = pickRandomTriplet(newLst, rand)
        val newColor = pickRandomColorForTriplet(triplet, rand)
        val delta = changeEnergy(triplet, newColor)
        if (delta < 0) {
          changeColor(triplet, newColor)
          stat.changedNeg += 1
        } else {
          val P = probability(delta, T)
          applyWithProbability(P, {
            changeColor(triplet, newColor)
            stat.changedPos += 1
          })
        }
      }
    }

    var result = newLst.map(triplet => newTripletUsingNewOpinions(opinions)(triplet))
    (result, stat)
  }

  def probability(delta: Double, T: Double): Double = {
    if (T == 0.0) 0.0 else math.pow(math.E, -delta / T)
  }

  def newTripletUsingNewOpinions(opinions: Map[VertexId, Array[collection.mutable.Map[Int, Int]]])(triplet: EdgeTriplet[VD, ED]): EdgeTriplet[VD, ED] = {
    val newTriplet = new EdgeTriplet[VD, ED]()
    newTriplet.srcId = triplet.srcId
    newTriplet.dstId = triplet.dstId
    newTriplet.attr = triplet.attr

    newTriplet.srcAttr = opinions(triplet.srcId).map(mmap => mmap.toMap)
    newTriplet.dstAttr = opinions(triplet.dstId).map(mmap => mmap.toMap)
    newTriplet
  }

  def swapColorsPure(index: Int, opinions: Map[VertexId, Array[collection.mutable.Map[Int, Int]]])(tripletA: EdgeTriplet[VD, ED],
                                                                                                   tripletB: EdgeTriplet[VD, ED]): Unit = {
    val colorA = tripletA.attr.ownOpinion
    val colorB = tripletB.attr.ownOpinion
    require(colorA != colorB)
    changePure(index, opinions)(tripletA, colorB)
    changePure(index, opinions)(tripletB, colorA)
  }

  def changePure(index: Int, opinions: Map[VertexId, Array[collection.mutable.Map[Int, Int]]])(triplet: EdgeTriplet[VD, ED], toColor: Int): Unit = {
    val fromColor = triplet.attr.ownOpinion
    require(fromColor != toColor)
    require(opinions(triplet.srcId)(triplet.attr.owner).get(fromColor).isDefined, "but it should be here")
    require(opinions(triplet.dstId)(triplet.attr.owner).get(fromColor).isDefined, "but it should be here")
    triplet.attr.updateOwnOpinion(toColor) //, index)
    val sourceMapa = opinions(triplet.srcId)(triplet.attr.owner)
    val destMapa = opinions(triplet.dstId)(triplet.attr.owner)

    changeColorInfo(sourceMapa, fromColor, toColor)
    changeColorInfo(destMapa, fromColor, toColor)
  }

  def deltaEnergySwapSquared(
    T: Double,
    triplets: Array[Triplet],
    opinions: Map[VertexId, Array[collection.mutable.Map[Int, Int]]])(tripletA: Triplet,
                                                                      tripletB: Triplet): Double = {
    val tripletAsrcAttrOwner = opinions.get(tripletA.srcId).get(tripletA.attr.owner)
    val tripletAdstAttrOwner = opinions.get(tripletA.dstId).get(tripletA.attr.owner)
    val tripletBsrcAttrOwner = opinions.get(tripletB.srcId).get(tripletB.attr.owner)
    val tripletBdstAttrOwner = opinions.get(tripletB.dstId).get(tripletB.attr.owner)

    val c = tripletA.attr.ownOpinion
    val cprime = tripletB.attr.ownOpinion
    val nuc = n(tripletAsrcAttrOwner, c, cprime)
    val nuv = n(tripletAdstAttrOwner, c, cprime)
    val nucprime = n(tripletBsrcAttrOwner, cprime, c)
    val nuvprime = n(tripletBdstAttrOwner, cprime, c)
    val delta = (1 / (numEdges.toDouble * (1 - 1 / N.toDouble))) * (nuc + nucprime + nuv + nuvprime)

    delta
  }

  def deltaEnergySwapCC(
    T: Double,
    triplets: Array[Triplet],
    opinions: Map[VertexId, Array[collection.mutable.Map[Int, Int]]])(tripletA: Triplet,
                                                                      tripletB: Triplet): Double = {
    val colorA = tripletA.attr.ownOpinion
    val colorB = tripletB.attr.ownOpinion

    val srcDataA = opinions.get(tripletA.srcId).get(tripletA.attr.owner).toMap
    val dstDataA = opinions.get(tripletA.dstId).get(tripletA.attr.owner).toMap
    val srcDataB = opinions.get(tripletB.srcId).get(tripletB.attr.owner).toMap
    val dstDataB = opinions.get(tripletB.dstId).get(tripletB.attr.owner).toMap

    val Eold = energyOfVertexCC(srcDataA) + energyOfVertexCC(dstDataA) + energyOfVertexCC(srcDataB) + energyOfVertexCC(dstDataB)

    val newSrcDataA = (srcDataA + (colorA -> (srcDataA.get(colorA).get - 1))) + (colorB -> (srcDataA.getOrElse(colorB, 0) + 1))
    val newDstDataA = (dstDataA + (colorA -> (dstDataA.get(colorA).get - 1))) + (colorB -> (dstDataA.getOrElse(colorB, 0) + 1))

    val newSrcDataB = (srcDataB + (colorB -> (srcDataB.get(colorB).get - 1))) + (colorA -> (srcDataB.getOrElse(colorA, 0) + 1))
    val newDstDataB = (dstDataB + (colorB -> (dstDataB.get(colorB).get - 1))) + (colorA -> (dstDataB.getOrElse(colorA, 0) + 1))

    val Enew = energyOfVertexCC(newSrcDataA) + energyOfVertexCC(newDstDataA) + energyOfVertexCC(newSrcDataB) + energyOfVertexCC(newDstDataB)

    val delta = Enew - Eold
    require(-8 <= delta && delta <= 8)
    normalize(delta)
  }

  def deltaEnergyChangeSquared(
    T: Double,
    triplets: Array[Triplet],
    opinions: Map[VertexId, Array[collection.mutable.Map[Int, Int]]])(triplet: Triplet, newColor: Int): Double = {
    val color = triplet.attr.ownOpinion

    val Auccprime = n(opinions.get(triplet.srcId).get(triplet.attr.owner), color, newColor)
    val Avccprime = n(opinions.get(triplet.dstId).get(triplet.attr.owner), color, newColor)
    val deltaT1 = (1 / (numEdges.toDouble * (1 - 1 / N))) * (Auccprime + Avccprime)
    deltaT1
  }

  def deltaEnergyChangeCC(
    T: Double,
    triplets: Array[Triplet],
    opinions: Map[VertexId, Array[collection.mutable.Map[Int, Int]]])(triplet: Triplet, newColor: Int): Double = {

    val color = triplet.attr.ownOpinion

    val srcData = opinions.get(triplet.srcId).get(triplet.attr.owner).toMap
    val dstData = opinions.get(triplet.dstId).get(triplet.attr.owner).toMap

    val Eold = energyOfVertexCC(srcData) + energyOfVertexCC(dstData)

    val newSrcData = (srcData + (color -> (srcData.get(color).get - 1))) + (newColor -> (srcData.getOrElse(newColor, 0) + 1))
    val newDstData = (dstData + (color -> (dstData.get(color).get - 1))) + (newColor -> (dstData.getOrElse(newColor, 0) + 1))

    val Enew = energyOfVertexCC(newSrcData) + energyOfVertexCC(newDstData)

    val delta = Enew - Eold
    require(-4 <= delta && delta <= 4)
    normalizeCC(delta)
  }

  def n(vertex: collection.mutable.Map[Int, Int], colorA: Int, colorB: Int): Double = {
    (vertex.getOrElse(colorA, 0) - vertex.getOrElse(colorB, 0) -1).toDouble / vertex.values.sum
  }

  def pickRandomTriplet(triplets: Array[Triplet], random: Random): Triplet = {
    triplets(random.nextInt(triplets.size))
  }
  def pickRandomColorForTriplet(triplet: Triplet, random: Random): Int = {
    var randomColor = random.nextInt(N)
    while (randomColor == triplet.attr.ownOpinion) {
      randomColor = random.nextInt(N)
    }

    require(0 <= randomColor && randomColor < N, "not a color, not good, saaad")
    randomColor
  }

  def randomNotConnectedTriplets(triplets: Array[Triplet], rand : Random): Option[(Triplet, Triplet)] = {
    val attemptLimit: Int = 100
    var tt: (Triplet, Triplet) = null
    var attempt = 0
    do {
      tt = randomPairTriplets(triplets, rand)
      attempt += 1
    } while (tripletsAreConnected(tt._1, tt._2) && attempt < attemptLimit)
    if (tripletsAreConnected(tt._1, tt._2)) None else Some(tt)
  }

  def tripletsAreConnected(a: Triplet, b: Triplet): Boolean = {
    Set(a.srcId, a.dstId, b.srcId, b.dstId).size != 4
  }

  def randomPairTriplets(triplets: Array[Triplet], rand : Random): (Triplet, Triplet) = {
    val aIndex = rand.nextInt(triplets.size)
    var bIndex = rand.nextInt(triplets.size)
    while (aIndex == bIndex) {
      bIndex = rand.nextInt(triplets.size)
    }
    (triplets(aIndex), triplets(bIndex))
  }
  def applyWithProbability(P: Double, f: => Unit)(implicit random: Random) {
    require(0 <= P && P <= 1, s"$P is not probability")
    if (random.nextDouble() <= P) {
      f
    }
  }

}

