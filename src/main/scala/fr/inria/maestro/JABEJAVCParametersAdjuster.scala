package fr.inria.maestro

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.graphx.Graph
import org.apache.spark.storage.StorageLevel
import org.apache.spark.graphx._
import org.apache.spark.graphx.impl.EdgePartitionBuilder
import java.io.PrintWriter
import java.io.FileWriter
import java.util.Random
import scala.collection.mutable.TreeSet

object JABEJAVCParametersAdjuster {
  def main(args: Array[String]): Unit = {
    val pathToGrpah = args(0)
    val filenameWithResult = args(1)

    val out = new PrintWriter(new FileWriter(filenameWithResult, false));

    val timesRepeat = 100

    val sc = new SparkContext(new SparkConf()
      .setSparkHome(System.getenv("SPARK_HOME"))
      .setJars(SparkContext.jarOfClass(this.getClass).toList))

    val result =
      (10 to 100 by 10).flatMap(minEdgePartitions => {
        (5 to 2 by -1).map(T => {

          val data = for (i <- 0 until timesRepeat) yield {
//        	  var graph: Graph[Int, Int] = GraphLoader.edgeListFile(sc, pathToGrpah, false,
//        			  edgeStorageLevel = StorageLevel.MEMORY_ONLY,
//        			  vertexStorageLevel = StorageLevel.MEMORY_ONLY,
//        			  minEdgePartitions = minEdgePartitions)
//            val jbjvc = new JABEJAVC(graph, minEdgePartitions, 20)

//            jbjvc.oneIteration(T)
          }

//          val reduced = data.reduce((x, y) => (x._1 + y._1, x._2 + y._2, x._3 + y._3, x._4 + y._4))
//          val averageSwapped = reduced._1.toDouble / timesRepeat
//          val averageLocalFunction = reduced._2.toDouble / timesRepeat
//          val averagePropagateValue = reduced._3.toDouble / timesRepeat
//          val averageRepartitioning = reduced._4.toDouble / timesRepeat
//          (minEdgePartitions, T, averageSwapped.toLong, averageLocalFunction.toLong, averagePropagateValue.toLong, averageRepartitioning.toLong)
        })
      }) //.sortBy { x => x.timeConsumption }

    for (r <- result) {
//      out.println(r._1 + ";" + r._2 + ";" + r._3 + ";" + r._4 + ";" + r._5 + ";" + r._6)
    }

    out.close()

    sc.stop()

  }

  case class Setting(averageSwapped: Long, averageTimeConsummed: Long, partitionNumber: Long = 0) {
    def timeConsumption = {
      averageTimeConsummed / averageSwapped
    }
    override def toString: String = {
      s"$partitionNumber; $averageTimeConsummed; $averageSwapped; $timeConsumption"
    }
  }
}