package fr.inria.maestro

import fr.inria.maestro.JABEJAVCutil._

import org.apache.spark.SparkContext._
import org.apache.spark._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.graphx._
import org.apache.spark.graphx.impl.EdgePartitionBuilder
import java.io.PrintWriter
import java.io.FileWriter
import java.util.Random
import scala.collection.mutable.TreeSet
import org.apache.log4j.Logger
import fr.inria.maestro.SA._

object SA {
  def main(args: Array[String]): Unit = {
    @transient implicit val loggr = Logger.getLogger("sa");

    val pathToGraph = args(0)
    val minEdgePartitions = args(1).toInt
    val T0 = args(2).toDouble
    val delta = args(3).toDouble
    val initialPartitioning = args(4)
    val outFolder = args(5)
    val alpha = args(6).toDouble
    val saveEnergy = args(7).toBoolean
    val lisone = args(8).toBoolean
    val mode = args(9)

    JABEJAVC.INITIAL_PARTITIONER = initialPartitioning

    val sc = new SparkContext(new SparkConf()
      .setSparkHome(System.getenv("SPARK_HOME"))
      .setJars(SparkContext.jarOfClass(this.getClass).toList))

    var graph: Graph[Int, Int] = GraphLoader.edgeListFile(sc, pathToGraph, false,
      edgeStorageLevel = StorageLevel.MEMORY_ONLY,
      vertexStorageLevel = StorageLevel.MEMORY_ONLY,
      minEdgePartitions = minEdgePartitions)

    var newGraph = assignColorsLikePartitioner(graph, JABEJAVC.INITIAL_PARTITIONER)
    val EbeforeGraph = convert(repartitionGraphBasedOnColor(newGraph, minEdgePartitions)).cache()
    EbeforeGraph.cache()
    saveToFileEdgesAsTheyPartitioned(EbeforeGraph, outFolder + "/before")

    val sa = new SA(graph, minEdgePartitions, alpha, saveEnergy, lisone, mode)

    val Ebefore = sa.E(EbeforeGraph)

    val (partitionedGraph, stats) = sa.repartition(sa.tempFunctionLinear(T0, delta), outFolder)

    if (saveEnergy) {
      sa.saveEnergiesToFile(stats.energies, outFolder + "/energies")
    }
    val Eafter = sa.E(partitionedGraph)
    saveToFileEdgesAsTheyPartitioned(partitionedGraph, outFolder + "/after")

    saveStatsToFile(outFolder + "/stats", pathToGraph, "fr.inria.maestro.SA", minEdgePartitions, T0, delta, sa.minimumT,
      initialPartitioning, 1, stats, Ebefore, Eafter, sa.L(partitionedGraph.numVertices.toInt, partitionedGraph.numEdges.toInt))

    sc.stop()

  }

  def randomNotConnectedTriplets(attemptLimit: Int = 100)(implicit triplets: Array[Triplet]): Option[(Triplet, Triplet)] = {
    var tt: (Triplet, Triplet) = null
    var attempt = 0
    do {
      tt = randomPairTriplets
      attempt += 1
    } while (tripletsAreConnected(tt._1, tt._2) && attempt < attemptLimit)
    Some(tt)
  }

  def tripletsAreConnected(a: Triplet, b: Triplet): Boolean = {
    Set(a.srcId, a.dstId, b.srcId, b.dstId).size != 4
  }

  def randomPairTriplets(implicit triplets: Array[Triplet]): (Triplet, Triplet) = {
    val random = new Random(System.currentTimeMillis())
    val aIndex = random.nextInt(triplets.size)
    var bIndex = random.nextInt(triplets.size)
    while (aIndex == bIndex) {
      bIndex = random.nextInt(triplets.size)
    }
    (triplets(aIndex), triplets(bIndex))
  }
  
}

class SA(graph: Graph[Int, Int],
         minEdgePartitions: Int, alpha: Double = 0.5,
         saveEnergy: Boolean,
         lisone: Boolean,
         mode: String) extends JABEJAVC(graph, minEdgePartitions, saveEnergy, lisone, frequencyOfRepartitioning = 1) {

  @transient implicit val loggr = Logger.getLogger("sa");

  override def EColor(localG: Graph[Int, Map[Int, Int]]): (Double, Double) = {
    val E = localG.numEdges.toDouble
    val N = minEdgePartitions.toDouble
    val C1 = 1.0 / (2 * E * (1 - 1 / N))
    val vertices = localG.mapReduceTriplets(triplet => {
      Iterator((triplet.srcId, triplet.attr), (triplet.dstId, triplet.attr))
    }, (a: Map[Int, Int], b: Map[Int, Int]) => zipMaps(a, b))
    val res = vertices.map(v => {
      val mapa = v._2
      mapa.values.map(value => value * value).sum.toDouble / mapa.values.sum
    }).reduce(_ + _)
    val T1 = C1 * (2 * E - res)
    val C2 = alpha / (E * E * (1 - 1 / N) * (1 - 1 / N))
    val colors = localG.edges.map(e => e.attr.head._1).countByValue()
    val T2 = C2 * colors.map(pair => math.pow(pair._2 - (E / N), 2)).sum

    (T1, T2)
  }

  override def E(localG: Graph[Int, Int]): (Double, Double) = {
    val edgs = localG.edges.mapPartitionsWithIndex((index, iterator) => {
      iterator.map(edge => {
        Edge(edge.srcId, edge.dstId, Map(index -> 1))
      })
    }, preservesPartitioning = true)
    val G = Graph.fromEdges(edgs, 0)
    EColor(G)
  }

  override def minimumT = {
    0.0
  }

  override def makeOneIterationInPartition(T: Double)(iterator: Iterator[EdgeTriplet[Map[Int, Int], Map[Int, Int]]])(implicit balance: Map[Int, Int], vertices_edges: (Int, Int)): (Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]], Stats) = {
    var diagnos = Stats()
    implicit val triplets = iterator.toArray

    implicit val random = new Random(System.currentTimeMillis())

    implicit val partitionNumber = balance.size

    var localBalance = collection.mutable.Map(balance.toSeq: _*)
    val l = L(vertices_edges._1, vertices_edges._2)
    for (i <- 0 until l) {
      mode match {
        case "SA" => {
          if(random.nextDouble() < 0.66){
//          if (random.nextBoolean()) { // tries to swap
            diagnos.swappedAttempt += 1
            val tt = randomNotConnectedTriplets()
            if (tt.isDefined) {
              val (a, b) = tt.get
              implicit val delta = deltaEnergy(a, b, vertices_edges._2)
              if (delta < 0) {
                swapColors2(a, b)
                diagnos.swappedNeg += 1
              } else {
                val P = math.pow(math.E, -delta / T)
                applyWithProbability(P, {
                  swapColors2(a, b)
                  diagnos.swappedPos += 1
                })(delta, T)
              }
            }
          } else { // tries to change 
            diagnos.changedAttempt += 1
            val triplet = pickRandomTriplet
            val newColor = pickRandomColorForTriplet(triplet)
            implicit val delta = deltaEnergy(triplet, newColor, localBalance, vertices_edges._2)
            if (delta < 0) {
              changeColor(triplet, newColor, Some(localBalance))
              diagnos.changedNeg += 1
            } else {
              val P = math.pow(math.E, -delta / T)
              applyWithProbability(P, {
                changeColor(triplet, newColor, Some(localBalance))
                diagnos.changedPos += 1
              })(delta, T)
            }
          }
        }
        case "SaT1" => {
          diagnos.swappedAttempt += 1
          val tt = randomNotConnectedTriplets()
          if (tt.isDefined) {
            val (a, b) = tt.get
            implicit val delta = deltaEnergy(a, b, vertices_edges._2, T)
            if (delta < 0) {
              swapColors2(a, b)
              diagnos.swappedNeg += 1
            }
          }
        }
        case "SAT1depcrecated" => {
          diagnos.swappedAttempt += 1
          val tt = randomNotConnectedTriplets()
          if (tt.isDefined) {
            val (a, b) = tt.get
            implicit val delta = deltaEnergy(a, b, vertices_edges._2)
            if (delta < 0) {
              swapColors2(a, b)
              diagnos.swappedNeg += 1
            } else {
              val P = math.pow(math.E, -delta / T)
              applyWithProbability(P, {
                swapColors2(a, b)
                diagnos.swappedPos += 1
              })(delta, T)
            }
          }
        }

      }
    }

    (triplets, diagnos)
  }

  def pickRandomTriplet(implicit triplets: Array[Triplet], random: Random): Triplet = {
    triplets(random.nextInt(triplets.size))
  }

  def pickRandomColorForTriplet(triplet: Triplet)(implicit partitionNumber: Int, random: Random): Int = {
    var randomColor = random.nextInt(partitionNumber)
    while (randomColor == triplet.attr.head._1) {
      randomColor = random.nextInt(partitionNumber)
    }
    randomColor
  }

  def applyWithProbability(P: Double, f: => Unit)(delta: Double, T: Double)(implicit random: Random) {
    require(0 <= P && P <= 1, s"$P is not probability where delta $delta and T $T")
    if (random.nextDouble() <= P) {
      f
    }
  }

  

//  def deltaEnergy(tripletA: Triplet, tripletB: Triplet, numEdges: Int, T : Double = 0.0)(implicit triplets: Array[Triplet]): Double = {
//    val E = numEdges.toDouble
//    val N = minEdgePartitions.toDouble
//    val c = tripletA.attr.head._1
//    val cprime = tripletB.attr.head._1
//    val nuc = n(tripletA.srcAttr, c, cprime, T)
//    val nuv = n(tripletA.dstAttr, c, cprime, T)
//    val nucprime = n(tripletB.srcAttr, cprime, c, T)
//    val nuvprime = n(tripletB.dstAttr, cprime, c, T)
//    val delta = (1 / (E * (1 - 1 / N))) * (nuc + nucprime + nuv + nuvprime)
//    require(-2 * (1 + alpha) < delta && delta < 2 * (1.0 + alpha), s"delta is $delta")
//
//    delta
//  }
//
//  def n(vertex: Map[Int, Int], colorA: Int, colorB: Int, T : Double): Double = {
//    (vertex.getOrElse(colorA, 0) - vertex.getOrElse(colorB, 0)*(1+T) - 1).toDouble / vertex.values.sum
//  }
  def deltaEnergy(triplet: Triplet, newColor: Int, balance: collection.mutable.Map[Int, Int], numEdges: Int)(implicit triplets: Array[Triplet]): Double = {

    val E = numEdges.toDouble
    val N = minEdgePartitions.toDouble

    val color = triplet.attr.head._1

    val Auccprime = n(triplet.srcAttr, color, newColor, 0)
    val Avccprime = n(triplet.dstAttr, color, newColor, 0)
    val deltaT1 = (1 / (E * (1 - 1 / N))) * (Auccprime + Avccprime)
    val deltaT2 = ((2 * alpha) / (E * E * (1 - 1 / N) * (1 - 1 / N))) * (balance.get(newColor).get - balance.get(color).get + 1)
    val delta = deltaT1 + deltaT2
    require(-2 * (1 + alpha) < delta && delta < 2 * (1 + alpha), s"delta is $delta")

    delta
  }
}