package fr.inria.maestro

import org.apache.spark.graphx.Graph
import org.apache.spark.graphx.Edge
import org.apache.spark.SparkContext._
import org.apache.spark.graphx._
import java.util.Random

//object Gibbs {
//  def main(args: Array[String]): Unit = {
//
//  }
//}
//
//case class EdgeData(var color: Int, var permanentPartition: Int)
//case class LocalVertexData(var localKnowledge: Map[Int, Int], var remoteKnowledge: Map[Int, Int])
//
//class Gibbs {
//  type VertexData = Map[Int, LocalVertexData]
//  type Triplet = EdgeTriplet[Map[Int, Int], Map[Int, Int]]
//
//  def getMapa(lst: List[EdgeData]): Map[Int, Int] = lst.groupBy(_.color).map(x => (x._1, x._2.length)).toMap
//
//  def makeVertexData(lst: List[EdgeData]): VertexData = {
//    var result = Map[Int, LocalVertexData]()
//
//    for (p <- lst.map(x => x.permanentPartition).toSet[Int]) {
//      var localKnowledge = getMapa(lst.filter(_.permanentPartition == p))
//      var remoteKnowledge = getMapa(lst.filter(_.permanentPartition != p))
//
//      result += (p -> LocalVertexData(localKnowledge, remoteKnowledge))
//    }
//
//    result
//  }
//
//  def P(data: Map[Int, Int]): Double = 0.3
//
//  def applyWithProbability(P: Double, f: => Unit) {
//    require(0 <= P && P <= 1, s"$P is not probability ")
//    if (new Random().nextDouble() <= P) {
//      f
//    }
//  }
//
//  def updateRemoteKnowledge(G: Graph[VertexData, EdgeData], pfunc: Option[Map[Int, Int] => Double] = None): Graph[VertexData, EdgeData] = {
//    val actualVertices = G.mapReduceTriplets(triplet => {
//      Iterator((triplet.srcId, List(triplet.attr)), (triplet.dstId, List(triplet.attr)))
//    }, (a: List[EdgeData], b: List[EdgeData]) => {
//      a ++ b
//    }).mapValues(x => makeVertexData(x))
//
//    G.outerJoinVertices(actualVertices)((id: VertexId, oldvalue: VertexData, newvalue: Option[VertexData]) => {
//      if (newvalue.isDefined) {
//        var result = Map[Int, LocalVertexData]()
//        for (p <- oldvalue.keys) {
//          val old = oldvalue(p)
//          val actual = newvalue.get(p)
//          var remoteKnowledge = old.remoteKnowledge
//          if (pfunc.isDefined) {
//            applyWithProbability(pfunc.get(old.localKnowledge), { remoteKnowledge = actual.remoteKnowledge })
//          } else {
//            remoteKnowledge = actual.remoteKnowledge
//          }
//          result += (p -> LocalVertexData(old.localKnowledge, remoteKnowledge))
//        }
//        result
//      } else oldvalue
//    })
//  }
//
//  val N = 10
//
//  def partition(): Graph[Int, Int] = {
//
//    var G: Graph[VertexData, EdgeData] = null
//
//    // assign color using partitioner
//    G = Graph.fromEdges(G.partitionBy(PartitionStrategy.fromString("HybridCut"))
//      .edges.mapPartitionsWithIndex((index, iterator) => {
//        iterator.map(edge => {
//          Edge(edge.srcId, edge.dstId, EdgeData(color = index, permanentPartition = 0))
//        })
//      }, preservesPartitioning = true), null)
//
//    // assign permanentPartition using partitioner
//    G = Graph.fromEdges(G.edges.mapPartitions(iterator => {
//      val rand = new Random()
//
//      iterator.map(e => {
//        val p = math.abs(rand.nextInt()) % N
//        val edge = Edge(e.srcId, e.dstId, EdgeData(color = e.attr.color, permanentPartition = p))
//        (p, edge)
//
//      })
//    }, preservesPartitioning = true).partitionBy(new IdentityPartitioner(N)).map(_._2), null)
//
//    // prepare to partitioning
//
//    updateRemoteKnowledge(G)
//    val steps = 10
//    for (s <- 0 until steps) {
//      // partitioning --- changing colors of L edges
//      val triplets = G.triplets.mapPartitionsWithIndex((index, iterator) => {
//        val random = new Random()
//        var triplets = iterator.toList
//        val L = 20000
//        for (i <- 0 until L) {
//          if (random.nextDouble() < 0.66) {
//            //          if (random.nextBoolean()) { // tries to swap
//            val tt = randomNotConnectedTriplets()
//            if (tt.isDefined) {
//              val (a, b) = tt.get
//              implicit val delta = deltaEnergy(a, b, vertices_edges._2)
//              if (delta < 0) {
//                swapColors2(a, b)
//              } else {
//                val P = math.pow(math.E, -delta / T)
//                applyWithProbability(P, {
//                  swapColors2(a, b)
//                })(delta, T)
//              }
//            }
//          } else { // tries to change 
//            val triplet = pickRandomTriplet
//            val newColor = pickRandomColorForTriplet(triplet)
//            implicit val delta = deltaEnergy(triplet, newColor, localBalance, vertices_edges._2)
//            if (delta < 0) {
//              changeColor(triplet, newColor, Some(localBalance))
//            } else {
//              val P = math.pow(math.E, -delta / T)
//              applyWithProbability(P, {
//                changeColor(triplet, newColor, Some(localBalance))
//              })(delta, T)
//            }
//          }
//        }
//
//        iterator.map(triplet => {
//          val e = new EdgeTriplet[VertexData, EdgeData]()
//          e.srcId = triplet.srcId
//          e.dstId = triplet.dstId
//          e.attr = triplet.attr
//          e.srcAttr = Map(index -> triplet.srcAttr(index))
//          e.dstAttr = Map(index -> triplet.dstAttr(index))
//          e
//        })
//      })
//
//      val vertices = triplets.flatMap(triplet => Iterator((triplet.srcId, triplet.srcAttr), (triplet.dstId, triplet.dstAttr))).reduceByKey(_ ++ _)
//      val edges = triplets.map(triplet => new Edge(triplet.srcId, triplet.dstId, triplet.attr))
//      G = Graph.fromEdges(edges, null)
//      G.outerJoinVertices(vertices)((id: VertexId, oldvalue: VertexData, newvalue: Option[VertexData]) => newvalue.get)
//
//      // acceptance ... 
//      updateRemoteKnowledge(G, Option(P _))
//    }
//
//    null
//  }
//  def randomNotConnectedTriplets(attemptLimit: Int = 100)(implicit triplets: Array[Triplet]): Option[(Triplet, Triplet)] = {
//    var tt: (Triplet, Triplet) = null
//    var attempt = 0
//    do {
//      tt = randomPairTriplets
//      attempt += 1
//    } while (tripletsAreConnected(tt._1, tt._2) && attempt < attemptLimit)
//    Some(tt)
//  }
//  
//  def randomPairTriplets(implicit triplets: Array[Triplet]): (Triplet, Triplet) = {
//    val random = new Random(System.currentTimeMillis())
//    val aIndex = random.nextInt(triplets.size)
//    var bIndex = random.nextInt(triplets.size)
//    while (aIndex == bIndex) {
//      bIndex = random.nextInt(triplets.size)
//    }
//    (triplets(aIndex), triplets(bIndex))
//  }
//  
//  def tripletsAreConnected(a: Triplet, b: Triplet): Boolean = {
//    Set(a.srcId, a.dstId, b.srcId, b.dstId).size != 4
//  }
//  
//  def deltaEnergy(triplet: Triplet, newColor: Int, balance: collection.mutable.Map[Int, Int], numEdges: Int)(implicit triplets: Array[Triplet]): Double = {
//
//    val E = numEdges.toDouble
//    val N = minEdgePartitions.toDouble
//
//    val color = triplet.attr.head._1
//
//    val Auccprime = n(triplet.srcAttr, color, newColor, 0)
//    val Avccprime = n(triplet.dstAttr, color, newColor, 0)
//    val deltaT1 = (1 / (E * (1 - 1 / N))) * (Auccprime + Avccprime)
//    val deltaT2 = ((2 * alpha) / (E * E * (1 - 1 / N) * (1 - 1 / N))) * (balance.get(newColor).get - balance.get(color).get + 1)
//    val delta = deltaT1 + deltaT2
//    require(-2 * (1 + alpha) < delta && delta < 2 * (1 + alpha), s"delta is $delta")
//
//    delta
//  }
//  def n(vertex: Map[Int, Int], colorA: Int, colorB: Int, T : Double): Double = {
//    (vertex.getOrElse(colorA, 0) - vertex.getOrElse(colorB, 0)*(1+T) - 1).toDouble / vertex.values.sum
//  }
//}