package fr.inria.maestro

import fr.inria.maestro.JABEJAVCutil._

import org.apache.spark.SparkContext._
import org.apache.spark._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.graphx._
import org.apache.spark.graphx.impl.EdgePartitionBuilder
import java.io.PrintWriter
import java.io.FileWriter
import java.util.Random
import scala.collection.mutable.TreeSet
import org.apache.log4j.Logger

object SALog {
  def main(args: Array[String]): Unit = {
    @transient implicit val loggr = Logger.getLogger("sa");

    val pathToGraph = args(0)
    val minEdgePartitions = args(1).toInt
    val T0 = args(2).toDouble
    val delta = args(3).toDouble
    val initialPartitioning = args(4)
    val outFolder = args(5)
    val alpha = args(6).toDouble
    val saveEnergy = args(7).toBoolean
    val lisone = args(8).toBoolean
    val mode = args(9)

    JABEJAVC.INITIAL_PARTITIONER = initialPartitioning

    val sc = new SparkContext(new SparkConf()
      .setSparkHome(System.getenv("SPARK_HOME"))
      .setJars(SparkContext.jarOfClass(this.getClass).toList))

    var graph: Graph[Int, Int] = GraphLoader.edgeListFile(sc, pathToGraph, false,
      edgeStorageLevel = StorageLevel.MEMORY_ONLY,
      vertexStorageLevel = StorageLevel.MEMORY_ONLY,
      minEdgePartitions = minEdgePartitions)

    var newGraph = assignColorsLikePartitioner(graph, JABEJAVC.INITIAL_PARTITIONER)
    val EbeforeGraph = convert(repartitionGraphBasedOnColor(newGraph, minEdgePartitions)).cache()
    EbeforeGraph.cache()
    saveToFileEdgesAsTheyPartitioned(EbeforeGraph, outFolder + "/before")

    val sa = new SA(graph, minEdgePartitions, alpha, saveEnergy, lisone, mode)

    val Ebefore = sa.E(EbeforeGraph)

    val (partitionedGraph, stats) = sa.repartition(sa.tempFunctionLinear(T0, delta), outFolder)

    if (saveEnergy) {
      sa.saveEnergiesToFile(stats.energies, outFolder + "/energies")
    }
    val Eafter = sa.E(partitionedGraph)
    saveToFileEdgesAsTheyPartitioned(partitionedGraph, outFolder + "/after")

    saveStatsToFile(outFolder + "/stats", pathToGraph, "fr.inria.maestro.SA", minEdgePartitions, T0, delta, sa.minimumT,
      initialPartitioning, 1, stats, Ebefore, Eafter, sa.L(partitionedGraph.numVertices.toInt, partitionedGraph.numEdges.toInt))

    sc.stop()

  }
  
  def tempFunctionLinear(T0: Double, maxRound : Int)(round: Int): Double = {
    if(round < maxRound){
      var T = T0
      for(r <- 0 until round){
        val delta = T0 / (math.log(1 + round))
        T -= delta
      }
      T
    }else{
      -100.0
    }
  }
}
