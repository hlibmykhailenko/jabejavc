package fr.inria.maestro

import fr.inria.maestro.JABEJAVCutil._

import org.apache.spark.SparkContext._
import org.apache.spark._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.graphx._
import org.apache.spark.graphx.impl.EdgePartitionBuilder
import java.io.PrintWriter
import java.io.FileWriter
import java.util.Random
import scala.collection.mutable.TreeSet
import org.apache.log4j.Logger

object SAGreedy {
  def main(args: Array[String]): Unit = {
    @transient implicit val loggr = Logger.getLogger("sa");

    val pathToGraph = args(0)
    val minEdgePartitions = args(1).toInt
    val T0 = args(2).toDouble
    val delta = args(3).toDouble
    val initialPartitioning = args(4)
    val outFolder = args(5)
    val alpha = args(6).toDouble
    val saveEnergy = args(7).toBoolean
    val lisone = args(8).toBoolean
    val mode = args(9)
    val round = args(10).toInt

    JABEJAVC.INITIAL_PARTITIONER = initialPartitioning

    val sc = new SparkContext(new SparkConf()
      .setSparkHome(System.getenv("SPARK_HOME"))
      .setJars(SparkContext.jarOfClass(this.getClass).toList))

    var graph: Graph[Int, Int] = GraphLoader.edgeListFile(sc, pathToGraph, false,
      edgeStorageLevel = StorageLevel.MEMORY_ONLY,
      vertexStorageLevel = StorageLevel.MEMORY_ONLY,
      minEdgePartitions = minEdgePartitions)

    var newGraph = assignColorsLikePartitioner(graph, JABEJAVC.INITIAL_PARTITIONER)
    val EbeforeGraph = convert(repartitionGraphBasedOnColor(newGraph, minEdgePartitions)).cache()
    EbeforeGraph.cache()
    saveToFileEdgesAsTheyPartitioned(EbeforeGraph, outFolder + "/before")

    val sa = new SAGreedy(graph, minEdgePartitions, alpha, saveEnergy, lisone, mode, round)

    val Ebefore = sa.E(EbeforeGraph)

    val (partitionedGraph, stats) = sa.repartition(sa.tempFunctionLinear(T0, delta), outFolder)

    if (saveEnergy) {
      sa.saveEnergiesToFile(stats.energies, outFolder + "/energies")
    }
    val Eafter = sa.E(partitionedGraph)
    saveToFileEdgesAsTheyPartitioned(partitionedGraph, outFolder + "/after")

    saveStatsToFile(outFolder + "/stats", pathToGraph, "fr.inria.maestro.SA", minEdgePartitions, T0, delta, sa.minimumT,
      initialPartitioning, 1, stats, Ebefore, Eafter, sa.L(partitionedGraph.numVertices.toInt, partitionedGraph.numEdges.toInt))

    sc.stop()

  }

  class SAGreedy(graph: Graph[Int, Int],
                 minEdgePartitions: Int, alpha: Double = 0.5,
                 saveEnergy: Boolean,
                 lisone: Boolean,
                 mode: String,
                 rounds : Int) extends SA(graph, minEdgePartitions, alpha, saveEnergy, lisone, mode) {
    override def repartition(tempFunction: (Int) => (Double), outFolder: String): (Graph[Int, Int], Stats) = {
      val startTime = System.currentTimeMillis()
      var G = assignColorsLikePartitioner(graph, JABEJAVC.INITIAL_PARTITIONER)

      G = repartitionGraphRandomly(G, minEdgePartitions)

      val out = new PrintWriter(new FileWriter(outFolder + "/iterations", true));

      implicit val vertices_edges = (graph.numVertices.toInt, graph.numEdges.toInt)
      implicit var round: Int = 0
      implicit var T = 0.0
      var totalSwappedNeg = 0l
      var totalSwappedPos = 0l
      var totalChangedNeg = 0l
      var totalChangedPos = 0l
      var totalSwappedAtt = 0l
      var totalChangedAtt = 0l
      var totalTimeForEnergy = 0l
      val buf = scala.collection.mutable.ListBuffer.empty[(Int, Double, Double, Double, Double)]
      for(i <- 0 until rounds){

        val stime = System.currentTimeMillis()

        val mrt = G.mapReduceTriplets(triplet => {
          Iterator((triplet.srcId, triplet.attr), (triplet.dstId, triplet.attr))
        }, (a: Map[Int, Int], b: Map[Int, Int]) => zipMaps(a, b))

        val tt = G.outerJoinVertices(mrt)((id: VertexId, oldvalue: Int, newvalue: Option[Map[Int, Int]]) => newvalue.get)

        implicit val balance = getBalance(tt)

        val valT = T
        val iterated = tt.triplets.mapPartitions(iterator => {
          Iterator(makeOneIterationInPartition(valT)(iterator))
        }, preservesPartitioning = true).cache()

        val itt = iterated.flatMap(x => x._1)

        totalSwappedPos += iterated.map(_._2.swappedPos).reduce(_ + _)
        totalChangedPos += iterated.map(_._2.changedPos).reduce(_ + _)
        totalSwappedNeg += iterated.map(_._2.swappedNeg).reduce(_ + _)
        totalChangedNeg += iterated.map(_._2.changedNeg).reduce(_ + _)
        totalSwappedAtt += iterated.map(_._2.swappedAttempt).reduce(_ + _)
        totalChangedAtt += iterated.map(_._2.changedAttempt).reduce(_ + _)

        val edges = itt.map(x => Edge(x.srcId, x.dstId, x.attr))

        G = Graph.fromEdges(edges, 0)

        iterated.unpersist(false)
        val etime = System.currentTimeMillis()
        val iterationtime = etime - stime

        G = repartitionGraphRandomly(G, minEdgePartitions)

        if (saveEnergy) {
          totalTimeForEnergy += applyWithTiming({
            val energy = EColor(G)
            val pair = (round, T, energy._1, energy._2, energy._1 + energy._2)
            buf += pair
          })
        }

        round += 1
        out.println(s"$round;$T")
        out.flush
      } 

      out.flush
      G = repartitionGraphBasedOnColor(G, minEdgePartitions)
      out.flush

      val endTime = System.currentTimeMillis()
      val totalTime = endTime - startTime - totalTimeForEnergy

      val resultG = convert(G)

      out.flush
      val stats = Stats()
      stats.swappedAttempt = totalSwappedAtt
      stats.swappedNeg = totalSwappedNeg
      stats.swappedPos = totalSwappedPos
      stats.changedAttempt = totalChangedAtt
      stats.changedNeg = totalChangedNeg
      stats.changedPos = totalChangedPos
      stats.spentTime = totalTime
      stats.energyTime = totalTimeForEnergy
      stats.energies = buf.toList
      stats.rounds = round
      out.println("4")
      out.flush
      (resultG, stats)
    }
  }

}
