package fr.inria.maestro

import fr.inria.maestro.JABEJAVCutil._

import org.apache.spark.SparkContext._
import org.apache.spark._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.graphx._
import org.apache.spark.graphx.impl.EdgePartitionBuilder
import java.io.PrintWriter
import java.io.FileWriter
import java.util.Random
import scala.collection.mutable.TreeSet

import org.apache.log4j.Logger;
import java.io.File

object JABEJAVC {
  var INITIAL_PARTITIONER = ""
  def main(args: Array[String]) {

    val pathToGraph = args(0)
    val minEdgePartitions = args(1).toInt
    val T0 = args(2).toDouble
    val delta = args(3).toDouble
    val initialPartitioning = args(4)
    val outFolder = args(5)
    val alpha = args(6)
    val saveEnergy = args(7).toBoolean
    val lisone = args(8).toBoolean

    INITIAL_PARTITIONER = initialPartitioning

    val sc = new SparkContext(new SparkConf()
      .setSparkHome(System.getenv("SPARK_HOME"))
      .setJars(SparkContext.jarOfClass(this.getClass).toList))

    var graph: Graph[Int, Int] = GraphLoader.edgeListFile(sc, pathToGraph, false,
      edgeStorageLevel = StorageLevel.MEMORY_ONLY,
      vertexStorageLevel = StorageLevel.MEMORY_ONLY,
      minEdgePartitions = minEdgePartitions)

    var newGraph: Graph[Int, Map[Int, Int]] = assignColorsLikePartitioner(graph, JABEJAVC.INITIAL_PARTITIONER)
    val beforeGraph = repartitionGraphBasedOnColor(newGraph, minEdgePartitions).cache
    saveToFileEdgesAsTheyPartitioned(convert(beforeGraph), outFolder + "/before")

    val jbjvc = new JABEJAVC(graph, minEdgePartitions, saveEnergy, lisone)

    val Ebefore = jbjvc.EColor(beforeGraph)
    
    val (partitionedGraph, stats) = jbjvc.repartition(jbjvc.tempFunctionLinear(T0, delta), outFolder)

    val Eafter = jbjvc.E(partitionedGraph)

    println(Ebefore + " aaaaaaaaaaaaaaaaaaaaaaaa " + Eafter)
    saveToFileEdgesAsTheyPartitioned(partitionedGraph, outFolder + "/after")

    if (saveEnergy) {
      jbjvc.saveEnergiesToFile(stats.energies, outFolder + "/energies")
    }
    saveStatsToFile(outFolder + "/stats", pathToGraph, "fr.inria.maestro.JABEJAVC",
      minEdgePartitions, T0, delta, jbjvc.minimumT, initialPartitioning,
      5, stats, Ebefore, Eafter, jbjvc.L(partitionedGraph.numVertices.toInt, partitionedGraph.numEdges.toInt))

    sc.stop()
  }

}

class JABEJAVC(graph: Graph[Int, Int],
               minEdgePartitions: Int,
               saveEnergy: Boolean,
               lisone: Boolean,
               frequencyOfRepartitioning: Int = 1) extends Serializable {
  
  
  @transient implicit val logger = Logger.getLogger("jabejavc");

  def tempFunctionLinear(T0: Double, delta: Double)(round: Int): Double = {
    T0 - delta * round
    //    math.max(minimumT, T0 - delta * round)
  }

  def saveEnergiesToFile(lst: List[(Int, Double, Double, Double, Double)], path: String): Unit = {
    val out = new PrintWriter(new FileWriter(new File(path)));
    for (l <- lst) {
      out.println(l._1 + "; " + l._2 + "; " + l._3 + "; " + l._4 + "; " + l._5)
    }
    out.close
  }

  def EColor(localG: Graph[Int, Map[Int, Int]]): (Double, Double) = {

    val E = localG.numEdges.toDouble
    val N = minEdgePartitions.toDouble
    val C1 = 1.0 / (2 * E * (1 - 1 / N))
    val vertices = localG.mapReduceTriplets(triplet => {
      Iterator((triplet.srcId, triplet.attr), (triplet.dstId, triplet.attr))
    }, (a: Map[Int, Int], b: Map[Int, Int]) => zipMaps(a, b))
    val res = vertices.map(v => {
      val mapa = v._2
      mapa.values.map(value => value * value).sum.toDouble / mapa.values.sum
    }).reduce(_ + _)
    val T1 = C1 * (2 * E - res)

    (T1, 0.0)
  }
  def E(localG: Graph[Int, Int]): (Double, Double) = {

    val edgs = localG.edges.mapPartitionsWithIndex((index, iterator) => {
      iterator.map(edge => {
        Edge(edge.srcId, edge.dstId, Map(index -> 1))
      })
    }, preservesPartitioning = true).cache()
    val G = Graph.fromEdges(edgs, 0).cache()
    val E = localG.numEdges.toDouble
    val N = minEdgePartitions.toDouble
    val C1 = 1.0 / (2 * E * (1 - 1 / N))
    val vertices = G.mapReduceTriplets(triplet => {
      Iterator((triplet.srcId, triplet.attr), (triplet.dstId, triplet.attr))
    }, (a: Map[Int, Int], b: Map[Int, Int]) => zipMaps(a, b))
    val res = vertices.map(v => {
      val mapa = v._2
      mapa.values.map(value => value * value).sum.toDouble / mapa.values.sum
    }).reduce(_ + _)
    val T1 = C1 * (2 * E - res)

    (T1, 0.0)
  }

  def oneIteration(T: Double = 2.0): (Long, Long, Long) = {
    var G = assignColorsLikePartitioner(graph, JABEJAVC.INITIAL_PARTITIONER)
    G = repartitionGraphRandomly(G, minEdgePartitions)

    val startTime = System.currentTimeMillis()

    var mrt = G.mapReduceTriplets(triplet => {
      Iterator((triplet.srcId, triplet.attr), (triplet.dstId, triplet.attr))
    }, (a: Map[Int, Int], b: Map[Int, Int]) => zipMaps(a, b))
    var tt = G.outerJoinVertices(mrt)((id: VertexId, oldvalue: Int, newvalue: Option[Map[Int, Int]]) => newvalue.get)
    val valT = T

    implicit val balance = getBalance(tt)
    implicit val vertices_edges = (graph.numVertices.toInt, graph.numEdges.toInt)

    val localFunctionStart = System.currentTimeMillis()

    val iterated = tt.triplets.mapPartitions(iterator => {
      Iterator(makeOneIterationInPartition(valT)(iterator))
    }, preservesPartitioning = true).cache()
    val itt = iterated.flatMap(x => x._1)
    val edges = itt.map(x => Edge(x.srcId, x.dstId, x.attr))
    G = Graph.fromEdges(edges, 0)
    iterated.unpersist(false)
    val localFunctionEnd = System.currentTimeMillis()
    val localFunctionTime = localFunctionEnd - localFunctionStart

    val propagateValuesStart = System.currentTimeMillis()
    mrt = G.mapReduceTriplets(triplet => {
      Iterator((triplet.srcId, triplet.attr), (triplet.dstId, triplet.attr))
    }, (a: Map[Int, Int], b: Map[Int, Int]) => zipMaps(a, b))
    tt = G.outerJoinVertices(mrt)((id: VertexId, oldvalue: Int, newvalue: Option[Map[Int, Int]]) => newvalue.get)
    tt.numEdges
    val propagateValuesEnd = System.currentTimeMillis()
    val propagateValuesTime = propagateValuesEnd - propagateValuesStart

    val repartitionStart = System.currentTimeMillis()
    G = repartitionGraphRandomly(G, minEdgePartitions)
    G.numEdges
    val repartitionEnd = System.currentTimeMillis()
    val repartitionTime = repartitionEnd - repartitionStart

    (localFunctionTime, propagateValuesTime, repartitionTime)
  }
  def repartition(tempFunction: (Int) => (Double), outFolder: String): (Graph[Int, Int], Stats) = {
    val startTime = System.currentTimeMillis()
    var G = assignColorsLikePartitioner(graph, JABEJAVC.INITIAL_PARTITIONER)

    G = repartitionGraphRandomly(G, minEdgePartitions)

    val out = new PrintWriter(new FileWriter(outFolder + "/iterations" , true));

    implicit val vertices_edges = (graph.numVertices.toInt, graph.numEdges.toInt)
    implicit var round: Int = 0
    implicit var T = tempFunction(round)
    var totalSwappedNeg = 0l
    var totalSwappedPos = 0l
    var totalChangedNeg = 0l
    var totalChangedPos = 0l
    var totalSwappedAtt = 0l
    var totalChangedAtt = 0l
    var totalTimeForEnergy = 0l
    val buf = scala.collection.mutable.ListBuffer.empty[(Int, Double, Double, Double, Double)]
    do {

      for (i <- 0 until frequencyOfRepartitioning) {
        val stime = System.currentTimeMillis()

        val mrt = G.mapReduceTriplets(triplet => {
          Iterator((triplet.srcId, triplet.attr), (triplet.dstId, triplet.attr))
        }, (a: Map[Int, Int], b: Map[Int, Int]) => zipMaps(a, b))

        val tt = G.outerJoinVertices(mrt)((id: VertexId, oldvalue: Int, newvalue: Option[Map[Int, Int]]) => newvalue.get)

        implicit val balance = getBalance(tt)

        val valT = T
        val iterated = tt.triplets.mapPartitions(iterator => {
          Iterator(makeOneIterationInPartition(valT)(iterator))
        }, preservesPartitioning = true).cache()

        val itt = iterated.flatMap(x => x._1)

        totalSwappedPos += iterated.map(_._2.swappedPos).reduce(_ + _)
        totalChangedPos += iterated.map(_._2.changedPos).reduce(_ + _)
        totalSwappedNeg += iterated.map(_._2.swappedNeg).reduce(_ + _)
        totalChangedNeg += iterated.map(_._2.changedNeg).reduce(_ + _)
        totalSwappedAtt += iterated.map(_._2.swappedAttempt).reduce(_ + _)
        totalChangedAtt += iterated.map(_._2.changedAttempt).reduce(_ + _)

        val edges = itt.map(x => Edge(x.srcId, x.dstId, x.attr))

        G = Graph.fromEdges(edges, 0)

        iterated.unpersist(false)
        val etime = System.currentTimeMillis()
        val iterationtime = etime - stime
      }

      G = repartitionGraphRandomly(G, minEdgePartitions)

      if (saveEnergy) {
        totalTimeForEnergy += applyWithTiming({
          val energy = EColor(G)
          val pair = (round, T, energy._1, energy._2, energy._1 + energy._2)
          buf += pair
        })
      }

      round += 1
      T = tempFunction(round)
      out.println(s"$round;$T")
      out.flush
    } while (T >= minimumT)

    out.println("1")
    out.flush
    G = repartitionGraphBasedOnColor(G, minEdgePartitions)
    out.println("2")
    out.flush
    
    val endTime = System.currentTimeMillis()
    val totalTime = endTime - startTime - totalTimeForEnergy
    
    val resultG = convert(G)
    
    out.println("3")
    out.flush
    val stats = Stats()
    stats.swappedAttempt = totalSwappedAtt
    stats.swappedNeg = totalSwappedNeg
    stats.swappedPos = totalSwappedPos
    stats.changedAttempt = totalChangedAtt
    stats.changedNeg = totalChangedNeg
    stats.changedPos = totalChangedPos
    stats.spentTime = totalTime
    stats.energyTime = totalTimeForEnergy
    stats.energies = buf.toList
    stats.rounds = round
    out.println("4")
    out.flush
    (resultG, stats)
  }

  def applyWithTiming(f: => Unit): Long = {
    val start = System.currentTimeMillis()
    f
    val end = System.currentTimeMillis()
    val time = end - start
    time
  }

  def minimumT = {
    0.0
  }

  def getBalance(graph: Graph[Map[Int, Int], Map[Int, Int]]): Map[Int, Int] = {
    graph.edges.map(e => e.attr.head._1).groupBy { identity }.map(x => (x._1, x._2.size)).collect.toMap
  }

  def print(G: Graph[Int, Map[Int, Int]]): Unit = {
    val ps = G.edges.map(e => e.attr.head._1).reduce(_ + _)
    logger.info("test message " + ps)
  }

  def makeOneIterationInPartition(T: Double)(iterator: Iterator[EdgeTriplet[Map[Int, Int], Map[Int, Int]]])(implicit balance: Map[Int, Int], vertices_edges: (Int, Int)): (Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]], Stats) = {
    implicit val triplets = iterator.toArray

    var diagnos = Stats()

    val buf = scala.collection.mutable.ListBuffer.empty[(Int, Double, Double, Double, Double)]

    val l = L(vertices_edges._1, vertices_edges._2)
    var swapCounter = 0
    for (i <- 0 until l) {

      var self: Option[VertexId] = getRandomNotInternalVertex

      var selfEdge: Option[EdgeTriplet[Map[Int, Int], Map[Int, Int]]] = None
      if (self.isDefined) {
        selfEdge = selectEdge(self.get)
      }
      var edgeConsidered = 0

    		  var swapped = false
      if (selfEdge.isDefined) {
        val possibles = scala.util.Random.shuffle(selectCandidates(self.get))
        // traverse all possibles

        var partnerEdge: Option[EdgeTriplet[Map[Int, Int], Map[Int, Int]]] = None
        
        for (partner <- possibles if isNotInternalLocally(partner) && !swapped) {
          edgeConsidered += getTripletsAttached(partner).length          
          partnerEdge = selectEdge(partner, self) // /TODO remove edges which connected to self vertex 

          if (partnerEdge.isDefined &&
            partnerEdge.get.attr.head._1 != selfEdge.get.attr.head._1
            && deltaEnergy(selfEdge.get, partnerEdge.get, vertices_edges._2, T) < 0) {
            swapColors2(selfEdge.get, partnerEdge.get)
            swapped = true
            swapCounter += 1
          }
        }
        
        diagnos.swappedAttempt += 1 
      }
      
    }
    diagnos.swappedNeg = swapCounter

    (triplets, diagnos)
  }
  
  def deltaEnergy(tripletA: Triplet, tripletB: Triplet, numEdges: Int, T : Double = 0.0)(implicit triplets: Array[Triplet]): Double = {
    val E = numEdges.toDouble
    val N = minEdgePartitions.toDouble
    val c = tripletA.attr.head._1
    val cprime = tripletB.attr.head._1
    val nuc = n(tripletA.srcAttr, c, cprime, T)
    val nuv = n(tripletA.dstAttr, c, cprime, T)
    val nucprime = n(tripletB.srcAttr, cprime, c, T)
    val nuvprime = n(tripletB.dstAttr, cprime, c, T)
    val delta = (1 / (E * (1 - 1 / N))) * (nuc + nucprime + nuv + nuvprime)

    delta
  }

  def n(vertex: Map[Int, Int], colorA: Int, colorB: Int, T : Double): Double = {
    (vertex.getOrElse(colorA, 0) - vertex.getOrElse(colorB, 0)*(1+T) - 1).toDouble / vertex.values.sum
  }

  def checkTripletPresence(triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]], srcId: VertexId, dstId: VertexId, color: Int): Int = {
    var notFound = true
    var answer = -1
    for (i <- 0 until triplets.length if notFound) {
      val triplet = triplets(i)
      if (triplet.srcId == srcId &&
        triplet.dstId == dstId &&
        triplet.attr.head._1 == color) {
        notFound = false
        answer = i
      }
    }
    answer
  }

  def selectEdge(vertex: VertexId, notConnect: Option[VertexId] = None)(implicit triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]]): Option[EdgeTriplet[Map[Int, Int], Map[Int, Int]]] = {
    var tripletsAttached = getTripletsAttached(vertex)
    if (notConnect.isDefined) {
      tripletsAttached = tripletsAttached.filter(x => x.srcId != notConnect.get && x.dstId != notConnect.get)
    }

    if (!tripletsAttached.isEmpty) {
      val color = tripletsAttached.map(triplet => triplet.attr.head._1)
        .groupBy { identity }
        .map(pair => (pair._1, pair._2.length))
        .toList
        .sortBy(x => x._2)
        .head._1
      val tripletsOfColor = tripletsAttached.filter(triplet => triplet.attr.head._1 == color).toArray
      Some(tripletsOfColor(math.abs(new Random().nextInt() % tripletsOfColor.length)))
    } else {
      None
    }
  }

  def L(numVertices: Int, numEdges: Int): Int = {
    if (lisone) {
      1
    } else {
//      val d = numEdges.toDouble / numVertices
//      val limit = numVertices.toDouble / (2 + 4 * d * minEdgePartitions * math.pow(math.E, -2 * d))
//      math.max(1, (limit / 100).toInt) // it was 100 before
      200
    }
  }

  def selectCandidates(vertex: VertexId)(implicit triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]]) = {
    // get neighbours 
    val neighbours = getNeighbourVertices(vertex)

    // and random one
    val anotherRandom = getRandomVertex(vertex)

    // possibles
    var possibles = neighbours
    if (anotherRandom.isDefined) {
      possibles = possibles :+ anotherRandom.get
    }

    possibles.toList
  }

  def swapColors2(tripletA: EdgeTriplet[Map[Int, Int], Map[Int, Int]],
                  tripletB: EdgeTriplet[Map[Int, Int], Map[Int, Int]]): Unit = {

    val colorA = tripletA.attr.head._1
    val colorB = tripletB.attr.head._1
    changeColor(tripletA, colorB, None)
    changeColor(tripletB, colorA, None)
  }

  def changeColor(triplet: Triplet, newColor: Int, balance: Option[collection.mutable.Map[Int, Int]]): Unit = {
    val color = triplet.attr.head._1
    if (balance.isDefined) {
      balance.get(color) = balance.get.get(color).get - 1
      balance.get(newColor) = balance.get.getOrElse(newColor, 0) + 1

    }
    var srcAttr = collection.mutable.Map(triplet.srcAttr.toSeq: _*)
    srcAttr(color) = srcAttr(color) - 1
    srcAttr(newColor) = srcAttr.getOrElseUpdate(newColor, 0) + 1
    triplet.srcAttr = srcAttr.toMap
    var dstAttr = collection.mutable.Map(triplet.dstAttr.toSeq: _*)
    dstAttr(color) = dstAttr(color) - 1
    dstAttr(newColor) = dstAttr.getOrElseUpdate(newColor, 0) + 1
    triplet.dstAttr = dstAttr.toMap
    triplet.attr = Map(newColor -> 1)
  }

  def equalTriplet(tripletA: EdgeTriplet[Map[Int, Int], Map[Int, Int]],
                   tripletB: EdgeTriplet[Map[Int, Int], Map[Int, Int]]): Boolean = {
    tripletA.srcId == tripletB.srcId && tripletA.dstId == tripletB.dstId && tripletA.attr.head._1 == tripletB.attr.head._1
  }

  def swapUtility(tripletA: EdgeTriplet[Map[Int, Int], Map[Int, Int]],
                  tripletB: EdgeTriplet[Map[Int, Int], Map[Int, Int]],
                  T: Double): Double = {
    val colorA = tripletA.attr.head._1
    val colorB = tripletB.attr.head._1
    (value(tripletA, colorB) + value(tripletB, colorA)) * T - (value(tripletA, colorA) + value(tripletB, colorB))
  }

  def value(triplet: EdgeTriplet[Map[Int, Int], Map[Int, Int]], color: Int): Double = {
    val ownColor = triplet.attr.head._1
    var substract = 0
    if (ownColor == color) {
      substract = 1
    }
    (triplet.srcAttr.getOrElse(color, 0) - substract).toDouble / (triplet.srcAttr.values.sum) +
      (triplet.dstAttr.getOrElse(color, 0) - substract).toDouble / (triplet.dstAttr.values.sum)
  }

}

