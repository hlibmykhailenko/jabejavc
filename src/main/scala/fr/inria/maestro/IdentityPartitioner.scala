package fr.inria.maestro

import org.apache.spark._

class IdentityPartitioner(partitions: Int) extends Partitioner {
  def numPartitions = partitions

  override def getPartition(key: Any): Int = key match {
    case null => 0
    case k: Int => if (0 <= k && k < numPartitions) {
      k
    } else {
      throw new Exception(s"key $key should be in [0, $numPartitions] range")
    }
  }
  
  override def equals(other: Any): Boolean = other match {
    case h: IdentityPartitioner =>
      h.numPartitions == numPartitions
    case _ =>
      false
  }

  override def hashCode: Int = numPartitions
}
