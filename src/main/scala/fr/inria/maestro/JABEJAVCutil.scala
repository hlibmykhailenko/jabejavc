package fr.inria.maestro

import java.util.Random
import java.io.PrintWriter

import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.SparkContext._
import java.io.FileWriter
import org.apache.log4j.Logger
import java.io.File

object JABEJAVCutil extends Serializable {

  def assignColorsLikePartitioner(graph: Graph[Int, Int], partitionerName: String): Graph[Int, Map[Int, Int]] = {
    val newGraph = graph.partitionBy(PartitionStrategy.fromString(partitionerName))
    val edges = newGraph.edges.mapPartitionsWithIndex((index, iterator) => {
      iterator.map(edge => {
        Edge(edge.srcId, edge.dstId, Map(index -> 1))
      })
    }, preservesPartitioning = true)
    Graph.fromEdges(edges, 0)
  }
  def printState(implicit round: Int, T: Double, newGraph: Graph[Int, Map[Int, Int]], out: PrintWriter) {
    out.println("======================START=====================================")
    val minEdgePartitions = newGraph.edges.partitionsRDD.count.toInt
    out.println(s"Round: $round, Temperature: $T, # of partitions: $minEdgePartitions.")
    out.println("Edges")
    for (p <- newGraph.edges.partitionsRDD.collect()) {
      val id = p._1
      out.println(s"EdgePartition ID: $id")
      for (edge <- p._2) {
        out.println("\t" + edge.srcId + " |--- " + edge.attr.head._1 + " ---> " + edge.dstId)
      }
    }
    out.println("======================END=======================================")
  }

  def convert(graph: Graph[Int, Map[Int, Int]]): Graph[Int, Int] = {
    graph.mapEdges((id, iterator) => iterator.map(triplet => triplet.attr.head._1))
  }

  def repartitionGraphRandomly(graph: Graph[Int, Map[Int, Int]], minEdgePartitions: Int)
  : Graph[Int, Map[Int, Int]] = {
    val edges = graph.edges.mapPartitions(iterator => {
      val rand = new Random()
      iterator.map(e => (math.abs(rand.nextInt()) % minEdgePartitions, e))
    }, preservesPartitioning = true).partitionBy(new IdentityPartitioner(minEdgePartitions)).map(_._2)
    Graph.fromEdges(edges, 0)
  }

  def repartitionGraphBasedOnColor(graph: Graph[Int, Map[Int, Int]], minEdgePartitions : Int): Graph[Int, Map[Int, Int]] = {
    val edges = graph.edges.map(x => {
      val newPartitionID = x.attr.head._1
      (newPartitionID, x)
    }).partitionBy(new IdentityPartitioner(minEdgePartitions)).map(x => x._2)
    Graph.fromEdges(edges, 0)
  }

  
  def saveToFileEdgesAsTheyPartitioned(graph: Graph[Int, Int], path: String): Unit = {
    val out = new PrintWriter(new FileWriter(path, false));

    val strings = graph.edges.mapPartitionsWithIndex((index, iterator) => {
      iterator.map(edge => {
        edge.srcId + "\t" + edge.dstId + "\t" + index
      })
    }, preservesPartitioning = true)

    //    strings.coalesce(1).saveAsTextFile(path)
    strings.collect.map(out.println)
    out.close
  }

  def getRandomNotInternalVertex(implicit triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]]): Option[VertexId] = {
    var notFound = true
    var result: Option[VertexId] = None
    for (i <- 0 until 10000 if notFound) {
      val v = getRandomVertex
//      val trs = getTripletsAttached(v)
//      val ppp = trs.map(printTriplet).mkString("\n")
//      val numColors = trs.map(x => x.attr.head._1).toSet.toArray
//      require(notFound == false, "just checking if it reaches this line " + v + " color " + numColors.size + " " + numColors(0) + "\n" + ppp + "\n")
      if (isNotInternalLocally(v)) {
        result = Some(v)
        notFound = false
      }
    }
    result
  }

  def isNotInternalLocally(vertex: VertexId)(implicit triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]]): Boolean = {
    getTripletsAttached(vertex).map(x => x.attr.head._1).toSet.size > 1
  }

  def getRandomVertex(another: VertexId)(implicit triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]]): Option[VertexId] = {
    var notFound = true
    var result: Option[VertexId] = None
    for (i <- 0 until 10000 if notFound) {
      val v = getRandomVertex
      if (isNotInternalLocally(v) & v != another) {
        result = Some(v)
        notFound = false
      }
    }
    result
  }

  def getRandomVertex(implicit triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]]): VertexId = {
    val random = new Random()
    val triplet = triplets(math.abs(random.nextInt() % triplets.length))
    if (random.nextBoolean()) {
      triplet.srcId
    } else {
      triplet.dstId
    }
  }

  def getNeighbourVertices(vertex: VertexId)(implicit triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]]): Array[VertexId] = {
    getTripletsAttached(vertex).map(x => {
      if (x.srcId == vertex)
        x.dstId
      else
        x.srcId
    }).toSet.toArray
  }
  def getTripletsAttached(vertex: VertexId)(implicit triplets: Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]]): Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]] = {
    triplets.filter(triplet => triplet.srcId == vertex || triplet.dstId == vertex)
  }

  def printTriplet(triplet: EdgeTriplet[Map[Int, Int], Map[Int, Int]]): String = {
    if (triplet != null) {
      triplet.srcId + "\t" + triplet.dstId + "\t" + triplet.attr.head._1
    } else {
      "null"
    }
  }

  def zipMaps(a: Map[Int, Int], b: Map[Int, Int]): Map[Int, Int] = {
    var zipped = scala.collection.mutable.Map[Int, Int]()
    val keys = a.keys.toSet ++ b.keys.toSet
    for (key <- keys) {
      val v = a.getOrElse(key, 0) + b.getOrElse(key, 0)
      zipped += (key -> v)
    }
    zipped.toMap
  }
  def saveStatsToFile(file: String, pathToGraph : String,
                    className : String, 
	                    N : Int, T0 : Double, delta : Double, Tend : Double, 
	                    initial : String, limitrepartitioning : Int, stats : Stats,
	                    Ebefore : (Double, Double), Eafter : (Double, Double), L : Long) : Unit = {
	  val out = new PrintWriter(new FileWriter(new File(file)));

	  out.println(s"graph : $pathToGraph")
	  out.println(s"class : $className")
    out.println(s"N : $N")
    out.println(s"Initial : $initial")
    out.println(s"T0 : $T0, delta : $delta, Tend : $Tend")
    out.println(s"Rounds : " + stats.rounds)
    out.println(s"L : $L")
    out.println(s"(limit : $limitrepartitioning)")
    out.println("Swapped Attempt: " + stats.swappedAttempt)
    out.println("Swapped Negative: " + stats.swappedNeg)
    out.println("Swapped Positive: " + stats.swappedPos)
    out.println("Changed Attempt: " + stats.changedAttempt)
    out.println("Changed Negative: " + stats.changedNeg)
    out.println("Changed Positive: " + stats.changedPos)
    
    out.println("Time spent: " + stats.spentTime)
    out.println("Time energy: " + stats.energyTime)
    
    out.println(s"Energey went from " + (Ebefore._1 + Ebefore._2) + " (" + Ebefore._1 + ", " + Ebefore._2 + ") to " + (Eafter._1 + Eafter._2) + " ( " + Eafter._1 + ","+ Eafter._2+ " ) ") 
    out.close

	}
  type Triplet = EdgeTriplet[Map[Int, Int], Map[Int, Int]]
  
  case class Stats(var swappedNeg: Long = 0,
            		   var swappedPos : Long = 0,
            		   var swappedAttempt : Long = 0,
                   var changedNeg: Long = 0,
                   var changedPos: Long = 0,
                   var changedAttempt : Long = 0,
                   var spentTime: Long = 0, 
                   var energyTime: Long = 0, 
                   var energies : List[(Int, Double, Double, Double, Double)] = List[(Int, Double, Double, Double, Double)](),
                   var rounds : Int = 0)
}