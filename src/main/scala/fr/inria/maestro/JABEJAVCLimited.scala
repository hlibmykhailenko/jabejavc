package fr.inria.maestro

import fr.inria.maestro.JABEJAVCutil._

import org.apache.spark.SparkContext._
import org.apache.spark._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.graphx._
import org.apache.spark.graphx.impl.EdgePartitionBuilder
import java.io.PrintWriter
import java.io.FileWriter
import java.util.Random
import scala.collection.mutable.TreeSet

import org.apache.log4j.Logger;
import java.io.File

object JABEJAVCLimited {
  var INITIAL_PARTITIONER = ""
  def main(args: Array[String]) {

    val pathToGraph = args(0)
    val minEdgePartitions = args(1).toInt
    val T0 = args(2).toDouble
    val delta = args(3).toDouble
    val initialPartitioning = args(4)
    val outFolder = args(5)
    val alpha = args(6)
    val saveEnergy = args(7).toBoolean
    val lisone = args(8).toBoolean

    JABEJAVC.INITIAL_PARTITIONER = initialPartitioning

    val sc = new SparkContext(new SparkConf()
      .setSparkHome(System.getenv("SPARK_HOME"))
      .setJars(SparkContext.jarOfClass(this.getClass).toList))

    var graph: Graph[Int, Int] = GraphLoader.edgeListFile(sc, pathToGraph, false,
      edgeStorageLevel = StorageLevel.MEMORY_ONLY,
      vertexStorageLevel = StorageLevel.MEMORY_ONLY,
      minEdgePartitions = minEdgePartitions)

    var newGraph: Graph[Int, Map[Int, Int]] = assignColorsLikePartitioner(graph, JABEJAVC.INITIAL_PARTITIONER)
    val beforeGraph = repartitionGraphBasedOnColor(newGraph, minEdgePartitions).cache
    saveToFileEdgesAsTheyPartitioned(convert(beforeGraph), outFolder + "/before")

    val jbjvc = new JABEJAVCLimited(graph, minEdgePartitions, saveEnergy, lisone)

    val Ebefore = jbjvc.EColor(beforeGraph)

    val (partitionedGraph, stats) = jbjvc.repartition(jbjvc.tempFunctionLinear(T0, delta), outFolder)

    val Eafter = jbjvc.E(partitionedGraph)

    saveToFileEdgesAsTheyPartitioned(partitionedGraph, outFolder + "/after")

    if (saveEnergy) {
      jbjvc.saveEnergiesToFile(stats.energies, outFolder + "/energies")
    }
    saveStatsToFile(outFolder + "/stats", pathToGraph, "fr.inria.maestro.JABEJAVCLimited",
      minEdgePartitions, T0, delta, jbjvc.minimumT, initialPartitioning,
      5, stats, Ebefore, Eafter, jbjvc.L(partitionedGraph.numVertices.toInt, partitionedGraph.numEdges.toInt))

    sc.stop()
  }

}

class JABEJAVCLimited(graph: Graph[Int, Int],
               minEdgePartitions: Int,
               saveEnergy: Boolean,
               lisone: Boolean,
               frequencyOfRepartitioning: Int = 1) extends JABEJAVC(graph, minEdgePartitions, saveEnergy, lisone, frequencyOfRepartitioning) {

  override def makeOneIterationInPartition(T: Double)(iterator: Iterator[EdgeTriplet[Map[Int, Int], Map[Int, Int]]])(implicit balance: Map[Int, Int], vertices_edges: (Int, Int)): (Array[EdgeTriplet[Map[Int, Int], Map[Int, Int]]], Stats) = {
    implicit val triplets = iterator.toArray

    var diagnos = Stats()

    val l = L(vertices_edges._1, vertices_edges._2)
    var swapCounter = 0
    
    var i = 0
    while (i < l) {

      var self: Option[VertexId] = getRandomNotInternalVertex

      var selfEdge: Option[EdgeTriplet[Map[Int, Int], Map[Int, Int]]] = None
      if (self.isDefined) {
        selfEdge = selectEdge(self.get)
      }
      var swapped = false
      if (selfEdge.isDefined) {
        val possibles = scala.util.Random.shuffle(selectCandidates(self.get))
        // traverse all possibles

        var partnerEdge: Option[EdgeTriplet[Map[Int, Int], Map[Int, Int]]] = None

        for (partner <- possibles if isNotInternalLocally(partner) && !swapped && i < l) {
          partnerEdge = selectEdge(partner, self) // /TODO remove edges which connected to self vertex 

          if (partnerEdge.isDefined &&
            partnerEdge.get.attr.head._1 != selfEdge.get.attr.head._1
            && deltaEnergy(selfEdge.get, partnerEdge.get, vertices_edges._2, T) < 0) {
            swapColors2(selfEdge.get, partnerEdge.get)
            swapped = true
            swapCounter += 1
          }
          
          i += 1
        }

        diagnos.swappedAttempt += 1
      }
      
      
      
      
    }
    diagnos.swappedNeg = swapCounter

    (triplets, diagnos)
  }

}

