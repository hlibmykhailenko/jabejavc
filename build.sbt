name := "jabejavc"

version := "1.0"

scalaVersion := "2.10.4"

autoScalaLibrary := true


libraryDependencies += "org.apache.spark" %% "spark-core" % "1.1.0"

libraryDependencies += "org.apache.spark" %% "spark-graphx" % "1.1.0"

libraryDependencies += "io.spray" %%  "spray-json" % "1.3.1"

libraryDependencies += "io.spray" %%  "spray-routing" % "1.3.1"

libraryDependencies += "com.typesafe.akka" %%  "akka-actor" % "2.2.3"

libraryDependencies += "it.unipd.dei" %% "graphx-diameter" % "0.1.0"

resolvers += "Akka Repository" at "http://repo.akka.io/releases/"

